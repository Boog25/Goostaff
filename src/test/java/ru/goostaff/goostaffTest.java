package ru.goostaff;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selectors;
import org.junit.jupiter.api.Test;

import java.nio.channels.Selector;
import java.time.Duration;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.*;

public class goostaffTest {


    @Test
    void shouldT05() {
        open("http://dev-app.goostaff.ru/");
        $("[type='text']").setValue("executor2@goostaff.ru");
        $("[name='password']").setValue("qqqwwweee");
        $("[type='submit']").click();
        $(byText("Рабочий стол")).shouldBe(Condition.visible, Duration.ofSeconds(18));
        $(byText("Новые")).click();
        $("[data-tab='new'][class='active']").shouldBe(Condition.visible, Duration.ofSeconds(5));
        $("[class='icon-logout']").click();

    }

    @Test
    void shouldT06() {
        open("http://dev-app.goostaff.ru/");
        $("[type='text']").setValue("executor2@goostaff.ru");
        $("[name='password']").setValue("qqqwwweee");
        $("[type='submit']").click();
        $(byText("Рабочий стол")).shouldBe(Condition.visible, Duration.ofSeconds(18));
        $(byText("В работе")).click();
        $("[data-tab='current'][class='active']").shouldBe(Condition.visible, Duration.ofSeconds(5));
        $("[class='icon-logout']").click();
    }

    @Test
    void shouldT07() {
        open("http://dev-app.goostaff.ru/");
        $("[type='text']").setValue("executor2@goostaff.ru");
        $("[name='password']").setValue("qqqwwweee");
        $("[type='submit']").click();
        $(byText("Рабочий стол")).shouldBe(Condition.visible, Duration.ofSeconds(18));
        $(byText("Завершенные")).click();
        $("[data-tab='complete'][class='active']").shouldBe(Condition.visible, Duration.ofSeconds(5));
        $("[class='icon-logout']").click();
    }

    @Test
    void shouldT08() {
        open("http://dev-app.goostaff.ru/");
        $("[type='text']").setValue("executor2@goostaff.ru");
        $("[name='password']").setValue("qqqwwweee");
        $("[type='submit']").click();
        $(byText("Рабочий стол")).shouldBe(Condition.visible, Duration.ofSeconds(18));
        $(byText("Арбитраж")).click();
        $("[data-tab='arbitrage'][class='active']").shouldBe(Condition.visible, Duration.ofSeconds(5));
        $("[class='icon-logout']").click();
    }

    @Test
    void shouldT09() {
        open("http://dev-app.goostaff.ru/");
        $("[type='text']").setValue("executor@goostaff.ru");
        $("[name='password']").setValue("qqqwwweee");
        $("[type='submit']").click();
        $(byText("Рабочий стол")).shouldBe(Condition.visible, Duration.ofSeconds(18));
        $(byText("Новые")).click();
        $("[class='text-nowrap']").click();
        $("[class='icon-leftArrow']").click();
        $("[data-tab='new'][class='active']").shouldBe(Condition.visible, Duration.ofSeconds(5));
        $("[class='icon-logout']").click();
    }

    @Test
    void shouldT10() {
        open("http://dev-app.goostaff.ru/");
        $("[type='text']").setValue("executor@goostaff.ru");
        $("[name='password']").setValue("qqqwwweee");
        $("[type='submit']").click();
        $(byText("Рабочий стол")).shouldBe(Condition.visible, Duration.ofSeconds(18));
        $(byText("В работе")).click();
        $("[class='text-nowrap']").click();
        $("[class='icon-leftArrow']").click();
        $("[data-tab='current'][class='active']").shouldBe(Condition.visible, Duration.ofSeconds(5));
        $("[class='icon-logout']").click();
    }

    @Test
    void shouldT11() {
        open("http://dev-app.goostaff.ru/");
        $("[type='text']").setValue("executor@goostaff.ru");
        $("[name='password']").setValue("qqqwwweee");
        $("[type='submit']").click();
        $(byText("Рабочий стол")).shouldBe(Condition.visible, Duration.ofSeconds(18));
        $(byText("Завершенные")).click();
        $("[class='text-nowrap']").click();
        $("[class='icon-leftArrow']").click();
        $("[data-tab='complete'][class='active']").shouldBe(Condition.visible, Duration.ofSeconds(5));
        $("[class='icon-logout']").click();
    }

    @Test
    void shouldT12() {
        open("http://dev-app.goostaff.ru/");
        $("[type='text']").setValue("executor@goostaff.ru");
        $("[name='password']").setValue("qqqwwweee");
        $("[type='submit']").click();
        $(byText("Рабочий стол")).shouldBe(Condition.visible, Duration.ofSeconds(18));
        $(byText("Арбитраж")).click();
        $("[class='text-nowrap']").click();
        $("[class='icon-leftArrow']").click();
        $("[data-tab='arbitrage'][class='active']").shouldBe(Condition.visible, Duration.ofSeconds(5));
        $("[class='icon-logout']").click();
    }

    @Test
    void shouldT16() {
        open("http://dev-app.goostaff.ru/");
        $("[type='text']").setValue("executor@goostaff.ru");
        $("[name='password']").setValue("qqqwwweee");
        $("[type='submit']").click();
        $(byText("Рабочий стол")).shouldBe(Condition.visible, Duration.ofSeconds(18));
        $(byText("Новые")).click();
        $("[class='text-nowrap']").click();
        $$("[class = 'btn btn-orange mr-1']").last().click();
        Configuration.timeout = 6000;
        $(Selectors.withText("Задание принято к исполнению")).shouldBe(Condition.visible, Duration.ofSeconds(18));
    }

    //    @Test
//    void shouldT17() {
//        open("http://dev-app.goostaff.ru/");
//        $("[type='text']").setValue("executor@goostaff.ru");
//        $("[name='password']").setValue("qqqwwweee");
//        $("[type='submit']").click();
//        $(byText("Рабочий стол")).shouldBe(Condition.visible, Duration.ofSeconds(18));
//        $(byText("Новые")).click();
//        $("[class='text-nowrap']").click();
//        $$("[class = 'btn btn-orange-inverted']").last().click();
//        Configuration.timeout = 6000;
//        $(Selectors.withText("Задание принято к исполнению"));
//    }

    @Test
    void shouldT18() {
        open("http://dev-app.goostaff.ru/");
        $("[type='text']").setValue("executor@goostaff.ru");
        $("[name='password']").setValue("qqqwwweee");
        $("[type='submit']").click();
        $(byText("Рабочий стол")).shouldBe(Condition.visible, Duration.ofSeconds(18));
        $(byText("В работе")).click();
        $("[class='text-nowrap']").click();
        $$("[class='btn btn-orange-inverted']").last().click();
        $("[name='comment']").setValue("Задание мне не подходит");
        $$("[class = 'btn btn-orange mt-1']").last().click();
        $(Selectors.withText("Задание аннуллировано")).shouldBe(Condition.visible,Duration.ofSeconds(18));
    }
    @Test
    void shouldT19() {
        open("http://dev-app.goostaff.ru/");
        $("[type='text']").setValue("executor@goostaff.ru");
        $("[name='password']").setValue("qqqwwweee");
        $("[type='submit']").click();
        $(byText("Рабочий стол")).shouldBe(Condition.visible, Duration.ofSeconds(18));
        $(byText("В работе")).click();
        $("[class='text-nowrap']").click();
        $$("[class='btn btn-orange mr-1']").last().click();
        $("[name='result']").setValue("Задание выполнено");
        $$("[type='file']").first().uploadFromClasspath("cv.pdf", "1.jpg", "2.png");
        $$("[type='text']").first().setValue("https://yandex.ru/");
        $("[class='ml-1']").click();
        $$("[type='text']").last().setValue("https://yandex.ru/");
        $("[class='btn btn-orange mt-1']").click();
        $(Selectors.withText("Ожидает приемки")).shouldBe(Condition.visible,Duration.ofSeconds(18));
    }
}


